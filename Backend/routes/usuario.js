const express = require('express');
const conexion = require('../conexion');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const fs = require('fs');
const Joi = require('@hapi/joi');
const jwt = require('jsonwebtoken');
const md5 = require("bcryptjs");
const nodemailer = require('nodemailer')
const cron = require('node-cron');
require('dotenv').config();
var auth = require('../services/autenticacion');
var checkRole = require('../services/checkrole');

router.use(express.json())

const schemaRegistro = Joi.object({
    nombreUsuario: Joi.string().required(),
    apellidoUsuario: Joi.string().required(),
    emailUsuario: Joi.string().min(6).max(255).required().email(),
    passwordUsuario: Joi.string().min(6).required(),
    puestoUsuario: Joi.string().required(),
    jefeDirectoUsuario: Joi.string().allow(null, ''),
    roleUsuario: Joi.string().required(),
    statusUsuario: Joi.string(),
    imagenUsuario: Joi.string()
})

const schemaActualizar = Joi.object({
    idUsuario: Joi.string().required(),
    nombreUsuario: Joi.string().required(),
    apellidoUsuario: Joi.string().required(),
    emailUsuario: Joi.string().min(6).max(255).required().email(),
    passwordUsuario: Joi.string().min(6).required(),
    puestoUsuario: Joi.string().required(),
    jefeDirectoUsuario: Joi.string().allow(null, ''),
    roleUsuario: Joi.string().required(),
    statusUsuario: Joi.string(),
    imagenUsuario: Joi.string()
})

function obtenerUltimoIdUsuario() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM usuarios";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`U${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioUsuario() {
    try {
        const id = await obtenerUltimoIdUsuario();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevoUsuario', async (req, res) => {
    try {
        const usuario = req.body;
        const { error } = schemaRegistro.validate(usuario);
        if (error) {
            return res.status(400).json({ error: error.details[0].message });
        }
        md5.hash(usuario.passwordUsuario, 10, async (err, hash) => {
            if (err) {
                return res.status(500).json({ mensaje: 'Error al encriptar la contraseña' });
            }
            const idUsuario = await idAleatorioUsuario();
            const query = "SELECT idUsuario, emailUsuario FROM usuarios WHERE emailUsuario=?";
            conexion.query(query, [usuario.emailUsuario], async (err, results) => {
                if (!err) {
                    if (results.length <= 0) {
                        const insertQuery = "INSERT INTO usuarios(idUsuario, nombreUsuario, apellidoUsuario, emailUsuario, passwordUsuario, puestoUsuario, jefeDirectoUsuario, roleUsuario, statusUsuario, imagenUsuario) VALUES(?,?,?,?,?,?,?,?,?,?)";
                        const valores = [idUsuario, usuario.nombreUsuario, usuario.apellidoUsuario, usuario.emailUsuario, hash, usuario.puestoUsuario, usuario.jefeDirectoUsuario, usuario.roleUsuario, usuario.statusUsuario, usuario.imagenUsuario];

                        conexion.query(insertQuery, valores, (err, results) => {
                            if (!err) {
                                return res.status(200).json({ mensaje: 'Registrado correctamente' });
                            } else {
                                if (err.sqlMessage.includes("Duplicate entry")) {
                                    return res.status(400).json({ mensaje: "Algo salió mal. Inténtalo de nuevo." });
                                } else {
                                    return res.status(500).json({ mensaje: err.sqlMessage });
                                }
                            }
                        });
                    } else {
                        return res.status(400).json({ mensaje: 'El email ya está registrado' });
                    }
                } else {
                    return res.status(500).json(err);
                }
            });
        });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ mensaje: 'Error interno del servidor' });
    }
});



router.post('/login', (req, res) => {
    const usuario = req.body;
    const emailUsuario = usuario.emailUsuario;
    const passwordUsuario = usuario.passwordUsuario;
    query = "SELECT emailUsuario, passwordUsuario, roleUsuario, statusUsuario, nombreUsuario, apellidoUsuario FROM usuarios WHERE emailUsuario=?";
    conexion.query(query, [emailUsuario], (err, results) => {
        if (!err) {
            if (results.length <= 0) {
                return res.status(401).json({ mensaje: "Email o Contraseña incorrectos" });
            } else {
                const storedPassword = results[0].passwordUsuario;
                md5.compare(passwordUsuario, storedPassword, (err, match) => {
                    if (err) {
                        return res.status(500).json({ mensaje: 'Error al comparar contraseñas' });
                    }
                    if (match) {
                        if (results[0].statusUsuario === 'Inactivo') {
                            return res.status(401).json({ mensaje: "Sin Acceso" });
                        }
                        const response = {
                            emailUsuario: results[0].emailUsuario,
                            roleUsuario: results[0].roleUsuario,
                            nombreUsuario: results[0].nombreUsuario,
                            apellidoUsuario: results[0].apellidoUsuario,
                        };
                        const accessToken = jwt.sign(response, process.env.ACCESS_TOKEN, { expiresIn: '5hr' });
                        return res.status(200).json({ token: accessToken });
                    } else {
                        return res.status(401).json({ mensaje: "Email o Contraseña incorrectos" });
                    }
                });
            }
        } else {
            return res.status(500).json(err);
        }
    });
});

// router.post('/login', (req, res) => {
//     const usuario = req.body;
//     query = "SELECT emailUsuario, passwordUsuario, roleUsuario, statusUsuario, nombreUsuario, apellidoUsuario FROM usuarios WHERE emailUsuario=?";
//     conexion.query(query, [usuario.emailUsuario], (err, results) => {
//         if (!err) {
//             if (results.length <= 0 || results[0].passwordUsuario != usuario.passwordUsuario) {
//                 return res.status(401).json({ mensaje: "Email o Contraseña incorrectos" });
//             }
//             else if (results[0].statusUsuario === 'Inactivo') {
//                 return res.status(401).json({ mensaje: "Sin Acceso" });
//             }
//             else if (results[0].passwordUsuario == usuario.passwordUsuario) {
//                 const response = { emailUsuario: results[0].emailUsuario, roleUsuario: results[0].roleUsuario };
//                 const accessToken = jwt.sign(response, process.env.ACCESS_TOKEN, { expiresIn: '5hr' });
//                 res.status(200).json({ token: accessToken });
//             }
//             else {
//                 return res.status(400).json({ mensaje: "Algo salió mal. Inténtalo de nuevo." });
//             }
//         }
//         else {
//             return res.status(500).json(err);
//         }
//     })
// })

router.get('/obtenerUsuarios', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    var query = "SELECT idUsuario, nombreUsuario, apellidoUsuario, emailUsuario, puestoUsuario, jefeDirectoUsuario, passwordUsuario, roleUsuario, statusUsuario, imagenUsuario FROM usuarios";
    conexion.query(query, (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerIdUsuario/:emailUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const email = req.params.emailUsuario;
    var query = "SELECT idUsuario FROM usuarios WHERE emailUsuario=?";
    conexion.query(query, [email], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerDatosUsuario/:emailUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const email = req.params.emailUsuario;
    var query = "SELECT * FROM usuarios WHERE emailUsuario=?";
    conexion.query(query, [email], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerRolesUsuario/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const id = req.params.idUsuario;
    var query = "SELECT * FROM roles WHERE idUsuario=?";
    conexion.query(query, [id], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.post('/nuevoRole', (req, res) => {
    const datos = req.body;
    let query = "INSERT INTO roles(tituloRole, idUsuario) VALUES(?,?)";

    conexion.query(query, [datos.tituloRole, datos.idUsuario], (err, results) => {
        if (!err) {
            return res.status(200).json({ mensaje: 'Registrado correctamente' });
        } else {
            return res.status(500).json({ mensaje: err });
        }
    });
});

router.delete('/eliminarRole/', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    const datos = req.body;
    var query = "DELETE FROM roles WHERE tituloRole=? AND idUsuario=?";
    conexion.query(query, [datos.tituloRole, datos.idUsuario], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(results)
            }
            return res.status(200).json({ mensaje: 'Eliminado correctamente' })
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/verificarToken', auth.autentificarToken, (req, res) => {
    return res.status(200).json({ mensaje: "true" })
})

router.patch('/eliminarUsuario/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    const id = req.params.idUsuario;
    var query = "UPDATE usuarios SET statusUsuario='Baja' WHERE idUsuario=?";
    conexion.query(query, [id], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(results)
            }
            return res.status(200).json({ mensaje: 'Eliminado correctamente' })
        } else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarNombreUsuario', auth.autentificarToken, checkRole.checkRole, async (req, res, next) => {
    let usuario = req.body;
    var query = "UPDATE usuarios set nombreUsuario=?, apellidoUsuario=? WHERE idUsuario=?";
    conexion.query(query, [usuario.nombreUsuario, usuario.apellidoUsuario, usuario.idUsuario], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            console.log(err);
            return res.status(500).json(err);
        }
    })
});

router.patch('/actualizarUsuario', auth.autentificarToken, checkRole.checkRole, async (req, res, next) => {
    try {
        let usuario = req.body;
        const { error } = schemaActualizar.validate(usuario);
        if (error) {
            return res.status(400).json({ error: error.details[0].message });
        }
        if (usuario.passwordUsuario) {
            const query = "SELECT passwordUsuario FROM usuarios WHERE emailUsuario=?";
            conexion.query(query, [usuario.emailUsuario], async (err, results) => {
                if (err) {
                    return res.status(500).json(err);
                }

                if (results.length > 0 && results[0].passwordUsuario !== usuario.passwordUsuario) {
                    const hash = await md5.hash(usuario.passwordUsuario, 10);
                    usuario.passwordUsuario = hash;
                }
                const updateQuery = "UPDATE usuarios SET nombreUsuario=?, apellidoUsuario=?, emailUsuario=?, passwordUsuario=?, puestoUsuario=?, jefeDirectoUsuario=?, roleUsuario=?, statusUsuario=? WHERE idUsuario=?";
                conexion.query(updateQuery, [usuario.nombreUsuario, usuario.apellidoUsuario, usuario.emailUsuario, usuario.passwordUsuario, usuario.puestoUsuario, usuario.jefeDirectoUsuario, usuario.roleUsuario, usuario.statusUsuario, usuario.idUsuario], (err, results) => {
                    if (!err) {
                        if (results.affectedRows == 0) {
                            return res.status(404).json({ mensaje: "No se encontró ningún usuario para actualizar" });
                        }
                        return res.status(200).json({ mensaje: "Actualizado correctamente" });
                    } else {
                        return res.status(500).json(err);
                    }
                });
            });
        } else {
            const updateQuery = "UPDATE usuarios SET nombreUsuario=?, apellidoUsuario=?, emailUsuario=?, puestoUsuario=?, jefeDirectoUsuario=?, roleUsuario=?, statusUsuario=? WHERE idUsuario=?";
            conexion.query(updateQuery, [usuario.nombreUsuario, usuario.apellidoUsuario, usuario.emailUsuario, usuario.puestoUsuario, usuario.jefeDirectoUsuario, usuario.roleUsuario, usuario.statusUsuario, usuario.idUsuario], (err, results) => {
                if (!err) {
                    if (results.affectedRows == 0) {
                        return res.status(404).json({ mensaje: "No se encontró ningún usuario para actualizar" });
                    }
                    return res.status(200).json({ mensaje: "Actualizado correctamente" });
                } else {
                    return res.status(500).json(err);
                }
            });
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({ mensaje: 'Error interno del servidor' });
    }
});



router.patch('/actualizarUsuarioEmail', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    const usuario = req.body;
    const { error } = schemaActualizar.validate(usuario);
    if (error) {
        return res.status(400).json({ error: error.details[0].message });
    }
    const checkEmailQuery = "SELECT emailUsuario FROM usuarios WHERE emailUsuario=?";
    conexion.query(checkEmailQuery, [usuario.emailUsuario], (err, existingUser) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ mensaje: 'Error interno del servidor' });
        }
        if (existingUser.length > 0) {
            return res.status(400).json({ mensaje: 'El email ya está registrado' });
        }

        if (usuario.passwordUsuario) {
            const query = "SELECT passwordUsuario FROM usuarios WHERE idUsuario=?";
            conexion.query(query, [usuario.idUsuario], async (err, [pass]) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ mensaje: 'Error interno del servidor' });
                }
                if (pass && pass.passwordUsuario !== usuario.passwordUsuario) {
                    const hash = await md5.hash(usuario.passwordUsuario, 10);
                    usuario.passwordUsuario = hash;

                    const updatePasswordQuery = "UPDATE usuarios SET passwordUsuario=? WHERE idUsuario=?";
                    conexion.query(updatePasswordQuery, [usuario.passwordUsuario, usuario.idUsuario], (err, results) => {
                        if (err) {
                            console.error(err);
                            return res.status(500).json({ mensaje: 'Error interno del servidor' });
                        }
                        if (results.affectedRows === 0) {
                            return res.status(404).json({ mensaje: "No se encontró ningún usuario para actualizar" });
                        }
                        // Actualiza los otros campos después de actualizar la contraseña
                        const updateQuery = "UPDATE usuarios SET nombreUsuario=?, apellidoUsuario=?, emailUsuario=?, puestoUsuario=?, jefeDirectoUsuario=?, roleUsuario=?, statusUsuario=? WHERE idUsuario=?";
                        conexion.query(updateQuery, [usuario.nombreUsuario, usuario.apellidoUsuario, usuario.emailUsuario, usuario.puestoUsuario, usuario.jefeDirectoUsuario, usuario.roleUsuario, usuario.statusUsuario, usuario.idUsuario], (err, results) => {
                            if (err) {
                                console.error(err);
                                return res.status(500).json({ mensaje: 'Error interno del servidor' });
                            }
                            if (results.affectedRows === 0) {
                                return res.status(404).json({ mensaje: "No se encontró ningún usuario para actualizar" });
                            }
                            return res.status(200).json({ mensaje: "Actualizado correctamente" });
                        });
                    });
                } else {
                    // Si no se actualiza la contraseña, simplemente actualiza los demás campos
                    const updateQuery = "UPDATE usuarios SET nombreUsuario=?, apellidoUsuario=?, emailUsuario=?, puestoUsuario=?, jefeDirectoUsuario=?, roleUsuario=?, statusUsuario=? WHERE idUsuario=?";
                    conexion.query(updateQuery, [usuario.nombreUsuario, usuario.apellidoUsuario, usuario.emailUsuario, usuario.puestoUsuario, usuario.jefeDirectoUsuario, usuario.roleUsuario, usuario.statusUsuario, usuario.idUsuario], (err, results) => {
                        if (err) {
                            console.error(err);
                            return res.status(500).json({ mensaje: 'Error interno del servidor' });
                        }
                        if (results.affectedRows === 0) {
                            return res.status(404).json({ mensaje: "No se encontró ningún usuario para actualizar" });
                        }
                        return res.status(200).json({ mensaje: "Actualizado correctamente" });
                    });
                }
            });
        } else {
            // Si no hay contraseña para actualizar, simplemente actualiza los demás campos
            const updateQuery = "UPDATE usuarios SET nombreUsuario=?, apellidoUsuario=?, emailUsuario=?, puestoUsuario=?, jefeDirectoUsuario=?, roleUsuario=?, statusUsuario=? WHERE idUsuario=?";
            conexion.query(updateQuery, [usuario.nombreUsuario, usuario.apellidoUsuario, usuario.emailUsuario, usuario.puestoUsuario, usuario.jefeDirectoUsuario, usuario.roleUsuario, usuario.statusUsuario, usuario.idUsuario], (err, results) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ mensaje: 'Error interno del servidor' });
                }
                if (results.affectedRows === 0) {
                    return res.status(404).json({ mensaje: "No se encontró ningún usuario para actualizar" });
                }
                return res.status(200).json({ mensaje: "Actualizado correctamente" });
            });
        }
    });
});






const storage = multer.diskStorage({
    destination: path.join(__dirname, '../imgs/usuarios/'),
    filename: (req, file, cb) => {
        const userId = req.params.id;
        const fileExtension = path.extname(file.originalname);
        const newFileName = `${userId}${fileExtension}`;
        cb(null, newFileName)
    }
});

const fileFilter = (req, file, cb) => {
    const allowedFileTypes = ['.png', '.jpg', '.jpeg', '.webp'];

    if (allowedFileTypes.includes(path.extname(file.originalname).toLowerCase())) {
        cb(null, true)
    } else {
        cb(new Error('Tipo de archivo no permitido'))
    }
}

const upload = multer({
    storage,
    fileFilter,
    limits: {
        fileSize: 2 * 1024 * 1024.
    },
});

router.patch('/cambiarPFP/:idUsuario', auth.autentificarToken, checkRole.checkRole, upload.single('imagen'), (req, res, next) => {
    const email = req.params.idUsuario;

    if (!req.file) {
        const error = new Error('No File')
        error.httpStatusCode = 400;
        return next(error)
    }
    const filePath = req.file.path;
    const nuevoNombre = email + path.extname(req.file.originalname);
    const nuevaRuta = path.join(path.dirname(filePath), nuevoNombre);
    fs.rename(filePath, nuevaRuta, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Error al cambiar el nombre del archivo' });
        }
        const filesImg = {

            id: null,
            nombre: nuevoNombre,
            imagen: filePath,
            fecha_creacion: null
        }
        var query = "UPDATE usuarios set imagenUsuario=? WHERE idUsuario=?";
        conexion.query(query, [filesImg.nombre, email], (err, results) => {
            if (!err) {
                if (results.affectedRows == 0) {
                    return res.status(404).json({ error: 'No se encontraron registros para actualizar' });
                }
                return res.status(200).json({ mensaje: "Actualizado correctamente" });
            }
            else {
                return res.status(500).json({ error: 'Error al actualizar la base de datos' });
            }
        })
    });
})


router.get('/obtenerActividades/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idUsuario = req.params.idUsuario;
    const query = "SELECT todos.idToDo, todos.tituloToDo, todos.statusToDo, todos.dueDateToDo, reuniones.nombreReunion FROM todos JOIN usuarios ON todos.idUsuario = usuarios.idUsuario JOIN reuniones ON todos.idReunion = reuniones.idReunion WHERE todos.idUsuario=?";

    conexion.query(query, [idUsuario], (err, results) => {
        if (!err) {
            if (results.length > 0) {
                const actividadesPorReunion = {};
                for (const actividad of results) {
                    const nombreReunion = actividad.nombreReunion;
                    if (!actividadesPorReunion[nombreReunion]) {
                        actividadesPorReunion[nombreReunion] = [];
                    }
                    actividadesPorReunion[nombreReunion].push({
                        nombreReunion: actividad.nombreReunion,
                        idToDo: actividad.idToDo,
                        tituloToDo: actividad.tituloToDo,
                        dueDateToDo: actividad.dueDateToDo,
                        statusToDo: actividad.statusToDo
                    });
                }
                res.json(actividadesPorReunion);
            } else {
                res.json([]);
            }
        } else {
            console.error('Error al obtener actividades:', err);
            res.status(500).json({ error: 'Error al obtener actividades', details: err.message });
        }
    });
});

router.get('/obtenerIndicadores/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idUsuario = req.params.idUsuario;
    const query = "SELECT measurables.idMeasurable, measurables.nombreMeasurable, measurables.tipoMedida, measurables.unidadesMeasurable, measurables.goalMetric, reuniones.nombreReunion FROM measurables JOIN usuarios ON measurables.idUsuario = usuarios.idUsuario JOIN reuniones ON measurables.idReunion = reuniones.idReunion WHERE measurables.idUsuario=? AND measurables.statusMeasurable = 'Activo'";

    conexion.query(query, [idUsuario], (err, results) => {
        if (!err) {
            if (results.length > 0) {
                const indicadoresPorReunion = {};
                for (const indicador of results) {
                    const nombreReunion = indicador.nombreReunion;
                    if (!indicadoresPorReunion[nombreReunion]) {
                        indicadoresPorReunion[nombreReunion] = [];
                    }
                    indicadoresPorReunion[nombreReunion].push({
                        idMeasurable: indicador.idMeasurable,
                        nombreMeasurable: indicador.nombreMeasurable,
                        unidadesMeasurable: indicador.unidadesMeasurable,
                        tipoMedida: indicador.tipoMedida,
                        goalMetric: indicador.goalMetric
                    });
                }
                res.json(indicadoresPorReunion);
            } else {
                res.json([]);
            }
        } else {
            console.error('Error al obtener indicador:', err);
            res.status(500).json({ error: 'Error al obtener indicadores', details: err.message });
        }
    });
});


async function obtenerActividades(idUsuario) {
    return new Promise((resolve, reject) => {
        var query = "SELECT todos.idToDo, todos.tituloToDo, todos.dueDateToDo, reuniones.nombreReunion FROM todos JOIN usuarios ON todos.idUsuario = usuarios.idUsuario JOIN reuniones ON todos.idReunion = reuniones.idReunion WHERE todos.idUsuario=? AND todos.statusToDo='Activo'";
        conexion.query(query, [idUsuario], (err, results) => {
            if (!err) {
                if (results.length > 0) {
                    const actividadesPorReunion = {};
                    for (const actividad of results) {
                        const nombreReunion = actividad.nombreReunion;
                        if (!actividadesPorReunion[nombreReunion]) {
                            actividadesPorReunion[nombreReunion] = [];
                        }
                        actividadesPorReunion[nombreReunion].push({
                            nombreReunion: actividad.nombreReunion,
                            tituloToDo: actividad.tituloToDo,
                            dueDateToDo: actividad.dueDateToDo
                        });
                    }
                    resolve(actividadesPorReunion);
                } else {
                    resolve([]);
                }
            } else {
                reject(err);
            }
        });
    });
}


// RECORDATORIO DE ACTIVIDADES

async function obtenerUsuarios() {
    return new Promise((resolve, reject) => {
        var query = "SELECT idUsuario, nombreUsuario, apellidoUsuario, statusUsuario, emailUsuario FROM usuarios WHERE statusUsuario='Activo'";
        conexion.query(query, (err, results) => {
            if (!err) {
                const ids = JSON.parse(JSON.stringify(results));
                resolve(ids);
            } else {
                reject(err);
            }
        });
    });
}

async function obtenerActividades(idUsuario) {
    return new Promise((resolve, reject) => {
        var query = "SELECT todos.idToDo, todos.tituloToDo, todos.dueDateToDo, reuniones.nombreReunion FROM todos JOIN usuarios ON todos.idUsuario = usuarios.idUsuario JOIN reuniones ON todos.idReunion = reuniones.idReunion WHERE todos.idUsuario=? AND todos.statusToDo='Activo'";
        conexion.query(query, [idUsuario], (err, results) => {
            if (!err) {
                if (results.length > 0) {
                    const actividadesPorReunion = {};
                    for (const actividad of results) {
                        const nombreReunion = actividad.nombreReunion;
                        if (!actividadesPorReunion[nombreReunion]) {
                            actividadesPorReunion[nombreReunion] = [];
                        }
                        actividadesPorReunion[nombreReunion].push({
                            nombreReunion: actividad.nombreReunion,
                            tituloToDo: actividad.tituloToDo,
                            dueDateToDo: actividad.dueDateToDo
                        });
                    }
                    resolve(actividadesPorReunion);
                } else {
                    resolve([]);
                }
            } else {
                reject(err);
            }
        });
    });
}

cron.schedule('0 10 * * *', () => {
    recordatorioActividades();
}, {
    timezone: 'America/Mexico_City'
});

cron.schedule('0 18 * * *', () => {
    recordatorioActividades();
}, {
    timezone: 'America/Mexico_City'
});

cron.schedule('01 10 * * *', () => {
    recordatorioActividadesDiarias()
}, {
    timezone: 'America/Mexico_City'
});

cron.schedule('01 18 * * *', () => {
    recordatorioActividadesDiarias()
}, {
    timezone: 'America/Mexico_City'
});

async function recordatorioActividades() {
    let actividades = []
    let datos = []
    try {
        datos = await obtenerUsuarios();
        for (let index = 0; index < datos.length; index++) {
            actividades = await obtenerActividades(datos[index].idUsuario)
            if (!(actividades.length == 0)) {
                enviarCorreo(datos[index], actividades, info => {
                })
            }
        }
    } catch (error) {
        console.log(error);
    }
}

async function enviarCorreo(correo, actividades, callback) {
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: "masterplanparautos@gmail.com",
            pass: "vllx kegv hhhm pshh",
        },
    })

    let htmlActividades = '<h2 style="text-align: center;">Actividades</h2>';
    for (const reunionKey in actividades) {
        if (actividades.hasOwnProperty(reunionKey)) {
            const reunion = actividades[reunionKey];
            htmlActividades += `<h3 style="font-weight: bold;">${reunion[0].nombreReunion}</h3>`;
            htmlActividades += '<ol>';
            for (const acts of reunion) {
                htmlActividades += `
                <li>
                    <div style="display: flex; justify-content: space-between;text-align:left;margin-bottom:8px;">
                        <div style="flex-grow: 1;">${acts.tituloToDo}</div>
                        <div style="margin-left: auto;">
                            <strong style="${tarde(acts.dueDateToDo) ? 'color: red;' : 'color: green;'}">${formatoFecha(acts.dueDateToDo)}</strong>
                        </div>
                    </div>
                </li>`;
            }
            htmlActividades += '</ol>';
        }
    }

    let mailOptions = {
        from: 'masterplanparautos@gmail.com',
        to: correo.emailUsuario,
        subject: `Actividades pendientes`,
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Actividades pendientes</title>
            <style>
                body {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 100vh;
                    margin: 0;
                }
                .container {
                    text-align: center;
                }     
                .card {
                    width: 80%; /* Ajustado para dispositivos más pequeños */
                    max-width: 600px; /* Establece un ancho máximo para dispositivos más grandes */
                    margin: 10px;
                    border: 1px solid #ddd;
                    border-radius: 8px;
                    padding: 20px;
                    display: inline-block;
                }
                @media only screen and (max-width: 600px) {
                    .card {
                        width: 90%; /* Ajusta el ancho para dispositivos más pequeños */
                    }
                }
            </style>
        </head>
        <body>
            <div class="container">
                <img src="cid:logo" width="100" alt="Logo">
                <h1>MasterPlan</h1>
                <h3>Hola! ${correo.nombreUsuario} ${correo.apellidoUsuario}, aquí están tus actividades pendientes.</h3>
                <div class="card">
                    ${htmlActividades}
                </div>
            </div>
        </body>
        </html>`,
        attachments: [
            {
                filename: 'logo.png',
                path: './imgs/correos/Logo.png',
                cid: 'logo'
            },
        ]
    }
    let info = await transporter.sendMail(mailOptions);
    callback(info)
}

function tarde(fecha) {
    const fechaHoy = new Date();
    const fechaParametro = new Date(fecha);
    return fechaParametro < fechaHoy;
}

function formatoFecha(fecha) {
    const fechaObj = new Date(fecha);
    const dia = fechaObj.getDate();
    const mes = fechaObj.getMonth() + 1;
    const año = fechaObj.getFullYear();
    return `${dia.toString().padStart(2, '0')}/${mes.toString().padStart(2, '0')}/${año}`;
}

async function recordatorioActividadesDiarias() {
    let datos = []
    try {
        datos = await obtenerUsuarios();
        for (let index = 0; index < datos.length; index++) {
            enviarCorreoActividades(datos[index], info => {
            })
        }
    } catch (error) {
        console.log(error);
    }
}

async function enviarCorreoActividades(correo, callback) {
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: "masterplanparautos@gmail.com",
            pass: "vllx kegv hhhm pshh",
        },
    })

    let mailOptions = {
        from: 'masterplanparautos@gmail.com',
        to: correo.emailUsuario,
        subject: `Actividades diarias`,
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Actividades pendientes</title>
            <style>
                body {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 100vh;
                    margin: 0;
                }
                .container {
                    text-align: center;
                }
                @media only screen and (max-width: 600px) {
                    .card {
                        width: 90%; /* Ajusta el ancho para dispositivos más pequeños */
                    }
                }
            </style>
        </head>
        <body>
            <div class="container">
                <img src="cid:logo" width="100" alt="Logo">
                <h1>MasterPlan</h1>
                <h2>Hola! ${correo.nombreUsuario} ${correo.apellidoUsuario}, recuerda actualizar tu calendario de actividades diarias!.</h2>
            </div>
        </body>
        </html>`,
        attachments: [
            {
                filename: 'logo.png',
                path: './imgs/correos/Logo.png',
                cid: 'logo'
            },
        ]
    }
    let info = await transporter.sendMail(mailOptions);
    callback(info)
}



module.exports = router;