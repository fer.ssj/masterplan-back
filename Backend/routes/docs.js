const express = require('express');
const conexion = require('../conexion');
const router = express.Router();
require('dotenv').config();
var auth = require('../services/autenticacion');
var checkRole = require('../services/checkrole');
const path = require('path');
const multer = require('multer');
const fs = require('fs');
const fse = require('fs-extra');
const Joi = require('@hapi/joi');
require('dotenv').config();

router.use(express.json())
let auxiliar

const storage = multer.diskStorage({
    destination: async (req, file, cb) => {
        let ruta = req.params.ruta;
        if (ruta == 'Sin ruta') {
            ruta = '';
        }
        const nombreArchivo = Buffer.from(file.originalname, 'ascii').toString('utf-8');
        try {
            let contador = 0;
            let nuevoNombre = nombreArchivo;
            while (await fse.pathExists(path.join(__dirname, '../archivos/', ruta, nuevoNombre))) {
                contador++;
                nuevoNombre = `${nombreArchivo.replace(/(\.\w+)?$/, '')} (${contador})${path.extname(nombreArchivo)}`;
            }
            auxiliar = nuevoNombre;
            cb(null, path.join(__dirname, '../archivos/', ruta));
        } catch (error) {
            cb(error);
        }
    },
    filename: (req, file, cb) => {
        if (auxiliar !== undefined) {
            cb(null, auxiliar);
        } else {
            const originalFileName = Buffer.from(file.originalname, 'ascii').toString('utf-8');
            cb(null, originalFileName);
        }
    }
});

const fileFilter = (req, file, cb) => {
    const allowedFileTypes = ['.pdf', '.xlsx', '.doc', '.docx', '.xml', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'];
    if (allowedFileTypes.includes(path.extname(file.originalname).toLowerCase())) {
        cb(null, true)
    } else {
        cb(new Error('Tipo de archivo no permitido'))
    }
}

const upload = multer({
    storage,
    fileFilter,
    limits: {
        fileSize: 2 * 1024 * 1024.
    },
});

router.post('/subirArchivo/:ruta', auth.autentificarToken, checkRole.checkRole, upload.single('archivo'), async (req, res, next) => {
    try {
        if (!req.file) {
            throw new Error('No File');
        }
        res.status(200).json('Archivo subido correctamente');
    } catch (error) {
        console.error('Error durante la subida del archivo:', error.message);
        res.status(500).json('Error durante la subida del archivo');
    }
});


router.get('/listaArchivos', (req, res) => {
    fs.readdir(path.join(__dirname, '../archivos'), (err, files) => {
        if (err) {
            return res.status(500).json({ error: 'Error al obtener la lista de archivos' });
        }
        return res.json({ archivos: files });
    });
});

router.get('/carpeta/:nombreCarpeta', (req, res) => {
    const nombreCarpeta = req.params.nombreCarpeta;
    const rutaCarpeta = path.join(__dirname, '../archivos', nombreCarpeta);

    fs.readdir(rutaCarpeta, (err, archivos) => {
        if (err) {
            return res.status(500).json({ error: 'Error al obtener la lista de archivos' });
        }
        return res.json({ archivos });
    });
});

router.post('/crearCarpeta', (req, res) => {
    const nombreCarpeta = req.body.aux;
    const rutaCarpeta = path.join(__dirname, '../archivos', nombreCarpeta);
    if (fs.existsSync(rutaCarpeta)) {
        return res.status(400).json({ error: 'Ya existe una carpeta con ese nombre' });
    }
    fs.mkdir(rutaCarpeta, (err) => {
        if (err) {
            return res.status(500).json({ error: 'Error al crear la carpeta' });
        }
        return res.status(200).json({ mensaje: 'Carpeta creada correctamente' });
    });
});

router.get('/archivos/:rutaArchivo', (req, res) => {
    const rutaArchivo = req.params.rutaArchivo;
    const rutaCompleta = path.join(__dirname, '../archivos', rutaArchivo);

    fs.stat(rutaCompleta, (err, stats) => {
        if (err || !stats.isFile()) {
            return res.status(404).json({ error: 'Archivo no encontrado' });
        }

        return res.download(rutaCompleta);
    });
});

router.post('/moverArchivo', auth.autentificarToken, checkRole.checkRole, async (req, res) => {
    const { origen, destino } = req.body;
    const rutaOrigen = path.join(__dirname, '../archivos', origen);
    const rutaDestino = path.join(__dirname, '../archivos', destino);
    try {
        const destinoExiste = await fse.pathExists(rutaDestino);
        if (destinoExiste) {
            const nombreBase = path.basename(destino);
            const nombreSinExtension = path.parse(nombreBase).name;
            const extension = path.extname(nombreBase);
            let contador = 0;
            let nuevoNombre = `${nombreSinExtension} (1)${extension}`;
            while (await fse.pathExists(path.join(path.dirname(rutaDestino), nuevoNombre))) {
                contador++;
                nuevoNombre = `${nombreSinExtension} (${contador})${extension}`;
            }
            const rutaDestinoConflicto = path.join(path.dirname(rutaDestino), nuevoNombre);
            await fse.copy(rutaOrigen, rutaDestinoConflicto, { overwrite: true });
            await fse.remove(rutaOrigen);
            return res.status(200).json({ mensaje: 'Contenido de la carpeta movido exitosamente (conflicto de nombres resuelto)' });
        }
        await fse.copy(rutaOrigen, rutaDestino, { overwrite: true });
        await fse.remove(rutaOrigen);
        res.status(200).json({ mensaje: 'Contenido de la carpeta movido exitosamente' });
    } catch (err) {
        console.error('Error al mover el contenido de la carpeta:', err);
        res.status(500).json({ error: 'Error al mover el contenido de la carpeta', details: err.message });
    }
});



router.post('/eliminarArchivo', auth.autentificarToken, checkRole.checkRole, async (req, res) => {
    const { elementoSeleccionado } = req.body;
    const rutaElemento = path.join(__dirname, '../archivos', elementoSeleccionado);
    try {
        await fse.remove(rutaElemento);
        res.status(200).json({ mensaje: 'Contenido de la carpeta movido exitosamente' });
    } catch (err) {
        console.error('Error al mover el contenido de la carpeta:', err);
        res.status(500).json({ error: 'Error al mover el contenido de la carpeta', details: err.message });
    }
});

router.post('/renombrarArchivo', auth.autentificarToken, checkRole.checkRole, async (req, res) => {
    const { original, nuevo } = req.body;
    const nombreAnterior = path.join(__dirname, '../archivos', original);
    const nuevoNombre = path.join(__dirname, '../archivos', nuevo);
    try {
        const nuevoExiste = await fse.pathExists(nuevoNombre);
        if (nuevoExiste) {
            return res.status(500).json({ mensaje: 'Ya existe un archivo con ese nombre' });
        }
        await fse.rename(nombreAnterior, nuevoNombre);
        res.status(200).json({ mensaje: 'Renombrado exitosamente' });
    } catch (err) {
        console.error('Error al renombrar:', err);
        res.status(500).json({ error: 'Error al renombrar', details: err.message });
    }
});






module.exports = router;
