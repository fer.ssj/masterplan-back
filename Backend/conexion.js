const mysql = require('mysql');
require('dotenv').config()

var conexion = mysql.createConnection({
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
});

conexion.connect((err) => {
    if(!err){
        console.log('Conexion exitosa');
    }
    else {
        console.log(err);
    }
});

module.exports = conexion;